# REACT TODOAPP

## Run
```
$ yarn run start:api
$ yarn run start
```

## API

**All TODOS**
```
# GET
http://localhost:5000/todos
```

**Create TODO**
```
# POST
http://localhost:5000/todos
{
  "text": "Buy new headphones"
}
```

**Toggle TODO**
```
# POST
http://localhost:5000/todos/:id/toggle
```
