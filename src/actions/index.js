export const setVisibilityFilter = filter => ({
  type: 'SET_VISIBILITY_FILTER',
  filter
})

export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
}


const url = 'http://localhost:5000/todos'

export const fetchTodos = () => {
  return dispatch => {
    return fetch(url)
      .then(response => response.json())
      .then(json => dispatch({
        type: 'RECEIVE_TODOS',
        todos: json.todos
      }))
  }
}

export const addTodo = (text) => {
  return dispatch => {
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify({ text }),
        headers: new Headers({ 'Content-Type': 'application/json' })
      })
      .then(response => response.json())
      .then(json => dispatch({
        type: 'ADD_TODO',
        id: json.todo.id,
        text: json.todo.text
      }))
  }
}

export const toggleTodo = (id) => {
  return dispatch => {
    return fetch(`${url}/${id}/toggle`, { method: 'POST' })
      .then(response => response.json())
      .then(json => dispatch({
        type: 'TOGGLE_TODO',
        id
      }))
  }
}
