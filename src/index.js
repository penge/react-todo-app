import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware, compose } from 'redux'
import { fetchTodos } from './actions'
import thunk from 'redux-thunk'
import rootReducer from './reducers'
import Root from './components/Root'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
)

store
  .dispatch(fetchTodos())
  .then(() => console.log(store.getState()))

render(
  <Root store={store} />,
  document.getElementById('root')
)
