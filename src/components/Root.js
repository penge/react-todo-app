import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import App from './App'

// To navigate in app use:
// <Link /> or <NavLink />


// Exaple of simple Root component
// that wraps Router in Provider
const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Route path="/todos" component={App} />
    </Router>
  </Provider>
)
 
Root.propTypes = {
  store: PropTypes.object.isRequired
}
 
export default Root