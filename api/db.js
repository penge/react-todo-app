module.exports = {
  todos: [
    {
      id: 1,
      text: 'Buy milk',
      completed: false
    },
    {
      id: 2,
      text: 'Go to cinema',
      completed: true,
    },
    {
      id: 3,
      text: 'Cook chicken',
      completed: true
    },
    {
      id: 4,
      text: 'Clean window',
      completed: false
    },
    {
      id: 5,
      text: 'Print documents',
      completed: false
    },
    {
      id: 6,
      text: 'Water flowers',
      completed: true
    }
  ]
}
