const express    = require('express')
const cors       = require('cors')
const bodyParser = require('body-parser')


// Express App
const app = express()
app.use(cors())
app.use(bodyParser.json())


// Todos DB
const db = require('./db.js')
const todos = db.todos


// List all Todos
app.get('/todos', (req, res) => {
  res.json({ todos })
})


// Create a new Todo
app.post('/todos', (req, res) => {
  let todoId = todos.length + 1
  let todo = {
    id: todoId,
    text: req.body.text,
    completed: false
  }
  todos.push(todo)
  res.json({ todo })
})


// Toggle a Todo
app.post('/todos/:id/toggle', (req, res) => {
  let todo = todos.find(todo => todo.id == req.params.id)
  todo.completed = !todo.completed
  res.json({ todo })
})


// Run server
app.listen(5000, () => {
  console.log('API listening on port 5000!')
})
